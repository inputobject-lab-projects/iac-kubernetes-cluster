output "nodes_ips" {
  value = xenorchestra_vm.vm_ubuntu[*].ipv4_addresses[0]
}

output "nodes" {
  value = xenorchestra_vm.vm_ubuntu[*].name_label
}

output "masters_ips" {
  value = xenorchestra_vm.vm_master_ubuntu[*].ipv4_addresses[0]
}

output "masters" {
  value = xenorchestra_vm.vm_master_ubuntu[*].name_label
}

output "mac_addresses" {
  value = concat(
    macaddress.vm_mac_ubuntu[*].id,
    macaddress.master_mac_ubuntu[*].id
  )
}