# docs : https://github.com/terra-farm/terraform-provider-xenorchestra/blob/master/docs/resources/vm.md
terraform {
  required_providers {
    xenorchestra = {
      source  = "terra-farm/xenorchestra"
      #version = "0.24.1"
    }
    macaddress = {
      source  = "ivoronin/macaddress"
      #version = "0.3.0"
    }
    rke = {
      source = "rancher/rke"
      #version = "1.4.1"
    }
  }
}