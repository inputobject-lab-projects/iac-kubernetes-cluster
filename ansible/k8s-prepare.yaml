---
- name: Main playbook to install K8s deps
  hosts: all
  become: yes
  ignore_errors: yes

  vars:
    kernel_modules:
      - br_netfilter
      - ip6_udp_tunnel
      - ip_set
      - ip_set_hash_ip
      - ip_set_hash_net
      - iptable_filter
      - iptable_nat
      - iptable_mangle
      - iptable_raw
      - nf_conntrack_netlink
      - nf_conntrack
      - nf_defrag_ipv4
      - nf_nat     
      - nfnetlink
      - udp_tunnel
      - veth
      - vxlan
      - x_tables
      - xt_addrtype
      - xt_conntrack
      - xt_comment
      - xt_mark
      - xt_multiport
      - xt_nat
      - xt_recent
      - xt_set
      - xt_statistic
      - xt_tcpudp

  tasks:
  - name: Upgrade to Centos Stream
    import_tasks: convert-centos8-to-stream.yaml
  
  - name: Install EPEL package
    package:
      name: 
        - epel-release
      state: present
    when: ansible_os_family == "RedHat"

  - name: Install python and pip
    import_tasks: install-python.yaml
    
  - name: Install docker
    import_tasks: install-docker.yaml

  - name: Install common packages
    package:
      name: 
        - lvm2
        - cloud-init
        - htop
      state: present
  
  - name: Install packages yum-specific packages
    package:
      name: 
        - device-mapper-persistent-data
        - net-snmp
        - net-snmp-utils
      state: present
    when: ansible_os_family == "RedHat"

  - name: Install packages apt-specific packages
    package:
      name: 
        - snmpd
        - xe-guest-utilities
      state: present
    when: ansible_os_family == "Debian"
  
  - name: Start xen agent
    systemd:
      name: xe-linux-distribution
      state: started
      enabled: true
  
  - name: Install NetData 
    shell: wget -O /tmp/netdata-kickstart.sh https://my-netdata.io/kickstart.sh && sh /tmp/netdata-kickstart.sh --nightly-channel --claim-token '{{ lookup('ansible.builtin.env', 'ANSIBLE_NETDATA_TOKEN') }}' --claim-url https://app.netdata.cloud

  - name: systemctl settings
    sysctl:
      name: "{{ item.name }}"
      value: "{{ item.value }}"
      sysctl_set: yes
      state: present
      reload: yes
    loop:
      - {name: net.ipv6.conf.all.disable_ipv6, value: 1}
      - {name: net.ipv6.conf.default.disable_ipv6, value: 1}
      - {name: net.ipv6.conf.lo.disable_ipv6, value: 1}
      - {name: net.ipv4.ip_forward, value: 1}
      - {name: net.bridge.bridge-nf-call-iptables, value: 1}
      - {name: user.max_user_namespaces, value: 15000}
  
- name: Setup firewall on the nodes
  import_playbook: setup-firewall.yaml

- name: Install NFS on the nodes
  import_playbook: install-nfs.yaml

- name: Install CA certificates on the nodes
  import_playbook: install-ca-certificate.yaml

- name: Install CIFS on the nodes
  import_playbook: install-cifs.yaml

- name: Install iSCSI on the nodes
  import_playbook: install-iscsi.yaml

- name: Enable firewalld masquerade
  import_playbook: enable-masquerade.yaml

- name: Mount longhorn storage
  import_playbook: add-longhorn-storage-disk.yaml