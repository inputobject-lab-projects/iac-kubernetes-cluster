1- Disable autonaming nics:

```
vi /boot/efi/EFI/xenserver/grub.cfg
```

Add `net.ifnames=0`

```
serial --unit=0 --speed=115200
terminal_input serial console
terminal_output serial console
set default=0
set timeout=5
net.ifnames=0
...
```

2- Add a rule (and create) `/etc/udev/rules.d/71-persistent-net.rules`

```
SUBSYSTEM=="net", ACTION=="add", DRIVERS=="?*", ATTR{address}=="20:7b:d2:6c:91:c4", ATTR{type}=="1", KERNEL=="eth*", NAME="eth1"
```

3- Remove unwanted networks (side-...)

```
xe pif-list params=uuid,device,host-name-label

...

xe pif-forget uuid=<uuid>
```
