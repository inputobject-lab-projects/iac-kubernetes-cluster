data "local_file" "rke_template_config" {
  filename = "templates/rke_template.yaml"
}

locals {
  alpha = ["a", "b", "c", "d"]
}


provider "rke" {
  log_file = "rke_debug.log"
}

resource "rke_cluster" "cluster" {
  cluster_name = "cluster-v3.${var.dns_sub_zone}.${var.dns_zone}"
  kubernetes_version = "v1.22.4-rancher1-1"
  upgrade_strategy {
      drain = true
      max_unavailable_worker = "20%"
  }
  services {
    etcd {}
    scheduler {}
    kubeproxy {}
    kube_api {
      always_pull_images = false
      pod_security_policy = true
      service_cluster_ip_range = "172.16.0.0/16"
      extra_args = {
        "kubelet-preferred-address-types" = "Hostname,InternalIP,ExternalIP"
      }
    }
    kube_controller {
      cluster_cidr = "172.20.0.0/16"
      service_cluster_ip_range = "172.100.0.0/16"
    }    
    kubelet {
      cluster_domain = "cluster.${var.dns_sub_zone}.${var.dns_zone}"
      cluster_dns_server = "172.16.0.10"
      fail_swap_on = false
      generate_serving_certificate = true
    }    
  }
  network {
    plugin = "canal"
  }
  ingress {
    provider = "none"
  }
  authentication {
    strategy = "x509"
    sans = [
      "controlplane.${var.dns_sub_zone}.${var.dns_zone}",
      "master-0.${var.dns_sub_zone}.${var.dns_zone}",
      "master-1.${var.dns_sub_zone}.${var.dns_zone}",
      "master-2.${var.dns_sub_zone}.${var.dns_zone}"
    ]
  }
  authorization {
    mode = "rbac"
  }
  
  dynamic "nodes" {
    for_each = xenorchestra_vm.vm_ubuntu

    content {
      address = nodes.value.name_description
      internal_address = nodes.value.name_description
      role = ["worker"]
      user = "cloud-user"
      docker_socket = "/run/docker.sock"
      ssh_key_path = "~/.ssh/id_rsa"
      labels = merge(var.node_labels, {"topology.kubernetes.io/region"="mtl-south-1","ntmax.ca/cloud-os"="ubuntu-20-04-focal"}) 
    }
  }

  dynamic "nodes" {
    for_each = xenorchestra_vm.vm_master_ubuntu

    content {
      address = nodes.value.name_description
      internal_address = nodes.value.name_description
      role = ["etcd", "controlplane"]
      user = "cloud-user"
      docker_socket = "/run/docker.sock"
      ssh_key_path = "~/.ssh/id_rsa"
      labels = merge(var.master_labels, {"topology.kubernetes.io/region"="mtl-south-1","ntmax.ca/cloud-os"="ubuntu-20-04-focal"})
    }
  }

  cert_dir = "files/generated/cluster_certs"
  custom_certs = true

  depends_on = [
    local_file.kube_admin_cert,
    local_file.kube_apiserver_proxy_client_cert,
    local_file.kube_apiserver_cert,
    local_file.kube_controller_manager_cert,
    local_file.kube_etcd_cert_ubuntu,
    local_file.kube_kubelet_master_cert_ubuntu,
    local_file.kube_kubelet_nodes_cert_ubuntu,
    local_file.kube_node_cert,
    local_file.kube_proxy_cert,
    local_file.kube_scheduler_cert,
    local_file.kube_service_account_token_cert,
    local_file.rke_config,
  ]
}

# We need to generate the config to be able to generate the csr to be able to generate the certs to be able to run the cluster setup
resource "local_file" "rke_config" {
  filename = "files/generated/rke_cluster_config.yml"
  content = join("", [yamlencode({
    "nodes" : concat([for i, e in xenorchestra_vm.vm_ubuntu[*] : {
      "address" : e.name_description,
      "internal_address" : e.name_description,
      "port" : "22",
      "role" : ["worker"],
      "user" : "cloud-user",
      "docker_socket" : "/run/docker.sock",
      "ssh_key_path" : "~/.ssh/id_rsa"
      "labels" : merge(var.node_labels, {"topology.kubernetes.io/region"="mtl-south-1","ntmax.ca/cloud-os"="ubuntu-20-04-focal", "topology.kubernetes.io/zone"="mtl-south-1${local.alpha[i % length(var.xen_large_sr_id)]}"})
      }
      ],[for i, e in xenorchestra_vm.vm_master_ubuntu[*] : {
      "address" : e.name_description,
      "internal_address" : e.name_description,
      "port" : "22",
      "role" : ["controlplane", "etcd"],
      "user" : "cloud-user",
      "docker_socket" : "/run/docker.sock",
      "ssh_key_path" : "~/.ssh/id_rsa"
      "labels" : merge(var.master_labels, {"topology.kubernetes.io/region"="mtl-south-1","ntmax.ca/cloud-os"="ubuntu-20-04-focal", "topology.kubernetes.io/zone"="mtl-south-1${local.alpha[i % length(var.xen_large_sr_id)]}"})
      }
      ])
    }),
    data.local_file.rke_template_config.content]
  )
}

resource "null_resource" "rke_generate_certs" {
  provisioner "local-exec" {
    command = "rke cert generate-csr --config files/generated/rke_cluster_config.yml --cert-dir files/generated/cluster_certs"
  }
  depends_on = [
    local_file.rke_config,
    null_resource.ansible
  ]

  triggers = {
    always_run = "${timestamp()}"
  }
}
