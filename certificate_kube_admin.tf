data "local_file" "kube_admin_csr" {
  filename = "files/generated/cluster_certs/kube-admin-csr.pem"

  depends_on = [
    null_resource.rke_generate_certs
  ]
}

resource "tls_locally_signed_cert" "kube_admin_cert" {
  cert_request_pem   = data.local_file.kube_admin_csr.content
  ca_private_key_pem = data.local_file.ca_private_key.content
  ca_cert_pem        = "${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"

  validity_period_hours = 87600

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
    "any_extended"
  ]
}

resource "local_file" "kube_admin_cert" {
  filename = "files/generated/cluster_certs/kube-admin.pem"
  content  = "${tls_locally_signed_cert.kube_admin_cert.cert_pem}${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"
}