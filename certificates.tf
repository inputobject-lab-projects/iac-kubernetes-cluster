data "local_file" "ca_private_key" {
  filename = "files/ca/ca-key.pem"
}

data "local_file" "ca_public_cert" {
  filename = "files/ca/ca.pem"
}

data "local_file" "domain_root_cert" {
  filename = "files/ca/domain-ca-certificate.pem"
}

resource "local_file" "kube_ca" {
  filename = "files/generated/cluster_certs/kube-ca.pem"
  content  = "${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"
}