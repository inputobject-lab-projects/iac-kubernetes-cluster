data "local_file" "kube_apiserver_csr" {
  filename = "files/generated/cluster_certs/kube-apiserver-csr.pem"

  depends_on = [
    null_resource.rke_generate_certs
  ]
}

resource "tls_locally_signed_cert" "kube_apiserver_cert" {
  cert_request_pem   = data.local_file.kube_apiserver_csr.content
  ca_private_key_pem = data.local_file.ca_private_key.content
  ca_cert_pem        = "${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"

  validity_period_hours = 87600

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
    "any_extended"
  ]
}

resource "local_file" "kube_apiserver_cert" {
  filename = "files/generated/cluster_certs/kube-apiserver.pem"
  content  = "${tls_locally_signed_cert.kube_apiserver_cert.cert_pem}${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"
}