/* resource "local_file" "kubeconfig" {
  content = rke_cluster.cluster.kube_config_yaml
  filename = "files/generated/kube_config_rke_cluster_config.yml"
}

resource "null_resource" "kubectl_apply" {
  provisioner "local-exec" {
    command = "kubectl apply -f files/manifests/ --recursive --kubeconfig files/generated/kube_config_rke_cluster_config.yml"
  }
  depends_on = [
    rke_cluster.cluster,
    null_resource.helmsman_apply,
    local_file.kubeconfig
  ]

  triggers = {
    always_run = "${timestamp()}"
  }
}

resource "null_resource" "helmsman_apply" {
  provisioner "local-exec" {
    command = "helmsman -f helmsman/helmsman.yaml -apply --kubeconfig files/generated/kube_config_rke_cluster_config.yml --subst-env-values --skip-validation"
  }
  depends_on = [
    local_file.kubeconfig
  ]

  triggers = {
    always_run = "${timestamp()}"
  }
}
 */