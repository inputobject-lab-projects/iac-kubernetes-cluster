resource "local_file" "ansible_inventory" {
  filename = "files/generated/ansible/inventory/hosts.yaml"
  content = yamlencode({
    "all" : {
      "vars" : {
        "ansible_user" : "cloud-user",
        "ansible_ssh_private_key_file" : "~/.ssh/id_rsa"
      },
      "children" : {
        "k8s-nodes" : {
          "hosts" : zipmap(xenorchestra_vm.vm_ubuntu[*].name_description, [for e in xenorchestra_vm.vm_ubuntu[*].name_description : {}])
        },
        "k8s-masters" : {
          "hosts" : zipmap(xenorchestra_vm.vm_master_ubuntu[*].name_description, [for e in xenorchestra_vm.vm_master_ubuntu[*].name_description : {}])
        }
      }
  } })
}

resource "null_resource" "ansible" {
  provisioner "local-exec" {
    command = "ansible-playbook -i files/generated/ansible/inventory/hosts.yaml ansible/k8s-prepare.yaml"
    #command = "echo skipping"
  }

  depends_on = [
    xenorchestra_vm.vm_ubuntu,
    xenorchestra_vm.vm_master_ubuntu,
    dns_a_record_set.masters,
    dns_a_record_set.nodes,
    local_file.ansible_inventory
  ]

  triggers = {
    always_run = "${timestamp()}"
  }
}