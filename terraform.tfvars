xen_network_name = "k8s.ntmax.ca"
xen_ubuntu_template_name = "Cloud-init - Ubuntu 20.04 Focal"
xen_pool_name = "Cluster-XCP-mini"

xen_sr_id = [
    "730fcff0-1a1d-a813-3da8-825be84b264b"
]
xen_large_sr_id = [
    "65de6e6a-1bd6-5278-3cc9-44c692d1b1c2",
    "6f5442b0-eefd-fbc5-e35d-182609c05c7b"
]

master_labels = {
    "ntmax.ca/cloud-platform" = "xcp-ng"
    "ntmax.ca/cloud-os"       = "ubuntu-20-04-focal"
    "ntmax.ca/region"         = "mtl-south-1"
}

node_labels = {
    "ntmax.ca/cloud-platform" = "xcp-ng"
    "ntmax.ca/cloud-os"       = "ubuntu-20-04-focal"
    "ntmax.ca/region"         = "mtl-south-1"
}

master_vm_tags = [
    "ntmax.ca/cloud-os:ubuntu-20-04-focal",
    "ntmax.ca/middleware:kubernetes",
    "ntmax.ca/provisionning:ansible",
    "ntmax.ca/provisionning:terraform",
    "kubernetes.io/role:master"
]
node_vm_tags = [
    "ntmax.ca/cloud-os:ubuntu-20-04-focal",
    "ntmax.ca/middleware:kubernetes",
    "ntmax.ca/provisionning:ansible",
    "ntmax.ca/provisionning:terraform",
    "kubernetes.io/role:worker"
]

certificate_params = {
    organization        = "NTMAX"
    organizational_unit = "Labs"
    locality            = "Montreal"
    country             = "CA"
    province            = "QC"
}