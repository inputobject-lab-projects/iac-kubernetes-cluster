provider "macaddress" {}

provider "xenorchestra" {
  # Configuration options
  # Must be ws or wss
  url = var.xen_xoa_url # Or set XOA_URL environment variable
  #username = "root"              # Or set XOA_USER environment variable
  #password = "<password>"              # Or set XOA_PASSWORD environment variable

  # This is false by default and
  # will disable ssl verification if true.
  # This is useful if your deployment uses
  # a self signed certificate but should be
  # used sparingly!
  insecure = true # Or set XOA_INSECURE environment variable to any value
}

data "local_file" "cloud_network_config" {
  filename = "templates/cloud_network_config.yaml"
}

# docs : https://github.com/terra-farm/terraform-provider-xenorchestra/blob/master/docs/resources/vm.md
data "xenorchestra_pool" "pool" {
  name_label = var.xen_pool_name
}

data "xenorchestra_hosts" "all_hosts" {
  pool_id = data.xenorchestra_pool.pool.id

  sort_by = "name_label"
  sort_order = "asc"
}

data "xenorchestra_network" "net" {
  name_label = var.xen_network_name
  pool_id    = data.xenorchestra_pool.pool.id
}

data "xenorchestra_template" "ubuntu" {
  name_label = var.xen_ubuntu_template_name
}

resource "random_uuid" "vm_ubuntu_id" {
  count = var.node_count
}

resource "random_uuid" "vm_master_ubuntu_id" {
  count = var.master_count
}

resource "macaddress" "vm_mac_ubuntu" {
  count  = var.node_count
  prefix = [0, 22, 62]
}

resource "macaddress" "master_mac_ubuntu" {
  count  = var.master_count
  prefix = [0, 22, 62]
}

resource "xenorchestra_cloud_config" "ansible_base_ubuntu" {
  count = var.node_count
  name  = "ubuntu-base-config-node-${count.index}"
  #template = data.local_file.cloud_config.content
  template = <<EOF
#cloud-config
hostname: "us20-k8s-${random_uuid.vm_ubuntu_id[count.index].result}.${var.dns_sub_zone}.${lower(var.dns_zone)}"

users:
  - name: cloud-user
    gecos: cloud-user
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ${var.vm_rsa_ssh_key}

packages:
  - xe-guest-utilities

EOF

  depends_on = [
    macaddress.vm_mac_ubuntu,
    macaddress.master_mac_ubuntu
  ]
}

resource "xenorchestra_cloud_config" "ansible_base_master_ubuntu" {
  count = var.master_count
  name  = "ubuntu-base-config-master-${count.index}"
  #template = data.local_file.cloud_config.content
  template = <<EOF
#cloud-config
hostname: "us20-k8s-${random_uuid.vm_master_ubuntu_id[count.index].result}.${var.dns_sub_zone}.${lower(var.dns_zone)}"

users:
  - name: cloud-user
    gecos: cloud-user
    shell: /bin/bash
    sudo: ALL=(ALL) NOPASSWD:ALL
    ssh_authorized_keys:
      - ${var.vm_rsa_ssh_key}

packages:
  - xe-guest-utilities

EOF

  depends_on = [
    macaddress.vm_mac_ubuntu,
    macaddress.master_mac_ubuntu
  ]
}

 
resource "xenorchestra_vm" "vm_ubuntu" {
  count = var.node_count

  name_label           = "us20-k8s-${random_uuid.vm_ubuntu_id[count.index].result}"
  cloud_config         = xenorchestra_cloud_config.ansible_base_ubuntu[count.index].template
  cloud_network_config = data.local_file.cloud_network_config.content
  template             = data.xenorchestra_template.ubuntu.id
  auto_poweron         = true

  name_description = "us20-k8s-${random_uuid.vm_ubuntu_id[count.index].result}.${var.dns_sub_zone}.${substr(lower(var.dns_zone), 0, length(var.dns_zone) - 1)}"

  network {
    network_id  = data.xenorchestra_network.net.id
    mac_address = macaddress.vm_mac_ubuntu[count.index].address
  }

  disk {
    #sr_id      = data.xenorchestra_sr.truenas_ssd[count.index % length(var.xen_sr_name)].id
    sr_id      = var.xen_sr_id[count.index % length(var.xen_sr_id)]
    name_label = "us20-k8s-${random_uuid.vm_ubuntu_id[count.index].result}"
    size       = var.vm_disk_size_gb * 1024 * 1024 * 1024 # GB to B
  }

  disk {
    #sr_id      = data.xenorchestra_sr.truenas_fasthdd[count.index % length(var.xen_large_sr_name)].id
    sr_id      = var.xen_large_sr_id[count.index % length(var.xen_large_sr_id)]
    name_label = "us20-k8s-${random_uuid.vm_ubuntu_id[count.index].result}-kubernetes-data"
    size       = var.vm_storage_disk_size_gb * 1024 * 1024 * 1024 # GB to B
  }

  cpus       = var.vm_cpu_count
  memory_max = var.vm_memory_size_gb * 1024 * 1024 * 1024 # GB to B

  wait_for_ip = true

  affinity_host = data.xenorchestra_hosts.all_hosts.hosts[count.index % length(data.xenorchestra_hosts.all_hosts.hosts)].id
  
  tags = concat(var.node_vm_tags, ["ntmax.ca/cloud-os:ubuntu-20-04-focal", "ntmax.ca/failure-domain:${count.index % length(data.xenorchestra_hosts.all_hosts.hosts)}"])

  lifecycle {
    ignore_changes = [ disk, template ]
  }
}

resource "xenorchestra_vm" "vm_master_ubuntu" {
  count = var.master_count

  name_label           = "us20-k8s-${random_uuid.vm_master_ubuntu_id[count.index].result}"
  cloud_config         = xenorchestra_cloud_config.ansible_base_master_ubuntu[count.index].template
  cloud_network_config = data.local_file.cloud_network_config.content
  template             = data.xenorchestra_template.ubuntu.id
  auto_poweron         = true
  name_description = "us20-k8s-${random_uuid.vm_master_ubuntu_id[count.index].result}.${var.dns_sub_zone}.${substr(lower(var.dns_zone), 0, length(var.dns_zone) - 1)}"

  network {
    network_id  = data.xenorchestra_network.net.id
    mac_address = macaddress.master_mac_ubuntu[count.index].address
  }

  disk {
    sr_id      = var.xen_sr_id[count.index % length(var.xen_sr_id)]
    
    #sr_id      = data.xenorchestra_sr.truenas_ssd.id
    name_label = "us20-k8s-${random_uuid.vm_master_ubuntu_id[count.index].result}"
    size       = var.master_disk_size_gb * 1024 * 1024 * 1024 # GB to B
  }

  cpus       = var.master_cpu_count
  memory_max = var.master_memory_size_gb * 1024 * 1024 * 1024 # GB to B

  wait_for_ip = true

  tags = concat(var.master_vm_tags, ["ntmax.ca/cloud-os:ubuntu-20-04-focal", "ntmax.ca/failure-domain:${count.index % length(data.xenorchestra_hosts.all_hosts.hosts)}"])

  affinity_host = data.xenorchestra_hosts.all_hosts.hosts[count.index % length(data.xenorchestra_hosts.all_hosts.hosts)].id
  lifecycle {
    ignore_changes = [ disk, affinity_host, template ]
  }
}