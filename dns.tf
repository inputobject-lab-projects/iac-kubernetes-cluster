# Configure the DNS Provider
provider "dns" {
  update {
    server = var.dns_server
    key_name      = var.dns_key_name
    key_algorithm = "hmac-sha512"
    key_secret    = var.dns_key_secret
    
  }
  alias = "bind"
}


# Create a DNS A record set
resource "dns_a_record_set" "nodes" {
  provider = dns.bind
  count     = var.node_count
  zone      = "${var.dns_sub_zone}.${var.dns_zone}"
  name      = "${xenorchestra_vm.vm_ubuntu[count.index].name_label}"
  addresses = xenorchestra_vm.vm_ubuntu[count.index].ipv4_addresses
  ttl       = 3600
}

resource "dns_cname_record" "nodes" {
  provider = dns.bind
  count = var.node_count
  zone  = "${var.dns_sub_zone}.${var.dns_zone}"
  name  = "node-${count.index}"
  cname = "${xenorchestra_vm.vm_ubuntu[count.index].name_label}.${var.dns_sub_zone}.${var.dns_zone}"
  ttl   = 900
}


# Create a DNS A record set
resource "dns_a_record_set" "masters" {
  provider = dns.bind
  count     = var.master_count
  zone      = "${var.dns_sub_zone}.${var.dns_zone}"
  name      = "${xenorchestra_vm.vm_master_ubuntu[count.index].name_label}"
  addresses = xenorchestra_vm.vm_master_ubuntu[count.index].ipv4_addresses
  ttl       = 3600
}

resource "dns_cname_record" "masters" {
  provider = dns.bind
  count = var.master_count
  zone  = "${var.dns_sub_zone}.${var.dns_zone}"
  name  = "master-${count.index}"
  cname = "${xenorchestra_vm.vm_master_ubuntu[count.index].name_label}.${var.dns_sub_zone}.${var.dns_zone}"
  ttl   = 900
}

resource "dns_a_record_set" "controlplane" {
  provider = dns.bind
  zone      = "${var.dns_sub_zone}.${var.dns_zone}"
  name      = "controlplane"
  addresses = xenorchestra_vm.vm_master_ubuntu[*].ipv4_addresses[0]
  ttl       = 3600
}