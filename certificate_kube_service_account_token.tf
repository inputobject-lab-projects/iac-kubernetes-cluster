# kube-service-account-token
resource "tls_private_key" "kube_service_account_token_key" {
  algorithm = "RSA"
}

resource "tls_cert_request" "kube_service_account_token_csr" {
  private_key_pem = tls_private_key.kube_service_account_token_key.private_key_pem

  subject {
    common_name         = "k8s.${var.dns_zone}"
    organizational_unit = "system:masters"
    organization        = var.certificate_params["organization"]
    locality            = var.certificate_params["locality"]
    province            = var.certificate_params["province"]
    country             = var.certificate_params["country"]
  }
}

resource "tls_locally_signed_cert" "kube_service_account_token_cert" {
  cert_request_pem   = tls_cert_request.kube_service_account_token_csr.cert_request_pem
  ca_private_key_pem = data.local_file.ca_private_key.content
  ca_cert_pem        = "${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"

  validity_period_hours = 87600

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
    "any_extended"
  ]
}

resource "local_file" "kube_service_account_token_cert" {
  filename = "files/generated/cluster_certs/kube-service-account-token.pem"
  content  = "${tls_locally_signed_cert.kube_service_account_token_cert.cert_pem}${data.local_file.ca_public_cert.content}\n${data.local_file.domain_root_cert.content}"
}

resource "local_file" "kube_service_account_token_key" {
  filename = "files/generated/cluster_certs/kube-service-account-token-key.pem"
  content  = tls_private_key.kube_service_account_token_key.private_key_pem
}