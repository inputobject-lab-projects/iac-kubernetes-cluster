terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/31913555/terraform/state/iac-cluster-xen"
    lock_address   = "https://gitlab.com/api/v4/projects/31913555/terraform/state/iac-cluster-xen/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/31913555/terraform/state/iac-cluster-xen/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }
}